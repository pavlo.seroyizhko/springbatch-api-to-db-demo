package com.agile.demobatchapitodb;

import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

public class RestReader implements ItemReader<PostsDTO> {

    private final String apiUrl;
    private final RestTemplate restTemplate;

    private int nextSemanticTermIndex;
    private List<PostsDTO> vocabularyData;


    public RestReader(String apiUrl, RestTemplate restTemplate) {
        this.apiUrl = apiUrl;
        this.restTemplate = restTemplate;
        this.nextSemanticTermIndex = 0;
    }

    private List<PostsDTO> fetchSemanticVocFromAPI() {
        ResponseEntity<PostsDTO[]> response = restTemplate.getForEntity(apiUrl,
                PostsDTO[].class
        );
        PostsDTO[] vocabulary = response.getBody();
        assert vocabulary != null;
        return Arrays.asList(vocabulary);
    }

    @Override
    public PostsDTO read() throws UnexpectedInputException, ParseException, NonTransientResourceException {
        if (this.vocabularyData == null) this.vocabularyData = fetchSemanticVocFromAPI();
        PostsDTO nextSemanticTerm = null;
        if (nextSemanticTermIndex < vocabularyData.size()) {
            nextSemanticTerm = this.vocabularyData.get(nextSemanticTermIndex);
            nextSemanticTermIndex++;
        }else {
            nextSemanticTermIndex = 0;
            this.vocabularyData = null;
        }

        return nextSemanticTerm;
    }
}
