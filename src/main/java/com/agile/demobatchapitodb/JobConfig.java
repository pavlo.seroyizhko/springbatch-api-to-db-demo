package com.agile.demobatchapitodb;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.web.client.RestTemplate;

import javax.sql.DataSource;

@Configuration
@EnableBatchProcessing
public class JobConfig {

    @Value("${rest.api.url}")
    String apiUrl;
    @Autowired
    StepBuilderFactory stepBuilderFactory;
    @Autowired
    JobBuilderFactory jobBuilderFactory;


    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
    @Bean
    @StepScope
    public ItemReader<PostsDTO> itemReader(RestTemplate restTemplate){

        return new RestReader(apiUrl, restTemplate);
    }

    @Bean
    public JdbcBatchItemWriter<PostsDTO> jdbcWriter(DataSource dataSource) {
        return new JdbcBatchItemWriterBuilder<PostsDTO>()
                .dataSource(dataSource)
                .sql("INSERT INTO vocabulary \n" +
                        "    (userId, id, body, title)\n" +
                        "values  (:userId, :id, :body, :title)")
                .beanMapped()
                .build();
    }
//    @Bean ItemWriter<CollibraSemanticTermDTO> itemWriter() {
//        return (items) -> items.forEach(System.out::println);
//    }


    @Bean
    public Step fromCollibraToDbStep() {
        return this.stepBuilderFactory.get("copyPasta")
                .<PostsDTO, PostsDTO>chunk(10)
                .reader(itemReader(null))
                .writer(jdbcWriter(null))
                .allowStartIfComplete(true)
                .build();
    }

    @Bean
    public Job job(Environment environment) {
        return this.jobBuilderFactory.get("mainJob")
                .incrementer(new RunIdIncrementer())
                .flow(fromCollibraToDbStep())
                .end()
                .build();
    }
}
