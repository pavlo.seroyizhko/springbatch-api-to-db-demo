package com.agile.demobatchapitodb;


import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.batch.test.context.SpringBatchTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@SpringBatchTest
@Slf4j
@AutoConfigureWireMock
class DemoBatchApiToDbApplicationTests {

    @Autowired
    private JobLauncherTestUtils jobLauncherTestUtils;

    @Autowired
    private DataSource dataSource;

    private JdbcTemplate jdbcTemplate;

    @BeforeEach
    public void setUp(){

        jdbcTemplate = new JdbcTemplate(this.dataSource);

        String schema = "DROP TABLE IF EXISTS vocabulary;\n" +
                "\n" +
                "CREATE TABLE vocabulary (\n" +
                "  userId Int NOT NULL,\n" +
                "  id Int default NULL,\n" +
                "  body  VARCHAR(255) default NULL,\n" +
                "  title  VARCHAR(255) default NULL,\n" +
                "  PRIMARY KEY (id)\n" +
                "  )";

        this.jdbcTemplate.execute(schema);

        String body = "[{\n" +
                "    \"userId\": 1,\n" +
                "    \"id\": 1,\n" +
                "    \"title\": \"sunt aut facere repellat provident occaecati excepturi optio reprehenderit\",\n" +
                "    \"body\": \"quia et suscipit\\nsuscipit recusandae consequuntur expedita et cum\\nreprehenderit molestiae ut ut quas totam\\nnostrum rerum est autem sunt rem eveniet architecto\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"userId\": 1,\n" +
                "    \"id\": 2,\n" +
                "    \"title\": \"qui est esse\",\n" +
                "    \"body\": \"est rerum tempore vitae\\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\\nqui aperiam non debitis possimus qui neque nisi nulla\"\n" +
                "  }]";
        stubFor(get(urlEqualTo("/api")).willReturn(aResponse()
                .withHeader("Content-Type", "application/json").withBody(body)));
    }


    @SneakyThrows
    @Test
    void testStep() {
        JobExecution jobExecution = jobLauncherTestUtils.launchStep("copyPasta");
        assertEquals(BatchStatus.COMPLETED, jobExecution.getStatus());
        List<Map<String, String>> results = this.jdbcTemplate.query("SELECT * FROM vocabulary", (rs, rowNum) -> {
            Map<String, String> item = new HashMap<>();
            item.put("userId", rs.getString("userId"));
            item.put("id", rs.getString("id"));
            return item;
        });

        Map<String, String> result = results.get(0);
        assertEquals(result.get("userId"), "1");
        assertEquals(result.get("id"), "1");

    }

    @SneakyThrows
    @Test
    void testJob() {
        JobExecution jobExecution =
                this.jobLauncherTestUtils.launchJob();
        assertEquals(BatchStatus.COMPLETED,
                jobExecution.getStatus());
    }
}
