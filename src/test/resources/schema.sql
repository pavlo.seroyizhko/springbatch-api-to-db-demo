DROP TABLE IF EXISTS vocabulary;

CREATE TABLE vocabulary (
  userId Int NOT NULL,
  id Int default NULL,
  title  VARCHAR(255) default NULL,
  body  VARCHAR(255) default NULL,
  PRIMARY KEY (id)
  );